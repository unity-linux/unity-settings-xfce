%define  debug_package %{nil}
%global xfceversion 4.13

Name:           unity-settings-xfce
Version:        4.13.6
Release:        2%{?dist}
Summary:        Settings Manager for Xfce

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://www.xfce.org/
#VCS git:git://git.xfce.org/xfce/xfce4-settings
Source0:        http://archive.xfce.org/src/xfce/xfce4-settings/%{xfceversion}/xfce4-settings-%{version}.tar.bz2
Source1:	%{name}-%{version}.tar.xz
# Use unity theme and font settings
Patch10:        xfce4-settings-4.12-unity.patch
#Patch0:         fix_scrolling_setting_dialogs.patch
# Needed to apply theme on start
Patch1:		xfce4-settings-4.13.5-xfsettingsd-use-replace.patch

BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  exo-devel >= 0.5.0
BuildRequires:  libxfce4ui-devel >= %{xfceversion}
BuildRequires:  libxfce4util-devel >= %{xfceversion}
BuildRequires:  xfconf-devel >= %{xfceversion}
BuildRequires:  libglade2-devel
BuildRequires:  libwnck3-devel
BuildRequires:  desktop-file-utils >= 0.7
BuildRequires:  libnotify-devel
BuildRequires:  libcanberra-devel
BuildRequires:  libxklavier-devel
%ifnarch s390 s390x
BuildRequires:	xorg-x11-drv-libinput-devel
%endif
BuildRequires:  libXrandr-devel
BuildRequires:  garcon-devel >= 0.1.10
Requires:       xfconf, arc-theme, xcape

Obsoletes:      xfce-mcs-manager < 4.4.3-2
Obsoletes:      xfce-mcs-plugins < 4.4.3-2
Obsoletes:      xfce-mcs-plugin-gsynaptics < 2.0-5
Obsoletes:      xfce-mcs-plugins-extra < 2.0-3
Obsoletes:      xfce4-gsynaptics-mcs-plugin < 1.0.0-3
Obsoletes:	xfce4-settings >= %{version}
Provides:	xfce4-settings = %{version}
Conflicts:	xfce4-settings >= %{version}

%description
This package includes the settings manager applications for the Xfce desktop. 

%prep
%setup -q -n xfce4-settings-%{version}
cd ..
tar xvJf %{SOURCE1}
cd xfce4-settings-%{version}
%patch10
#%patch0 -p1
%patch1 -p1


%build
%configure --enable-sound-settings --enable-pluggable-dialogs --enable-maintainer-mode --enable-xorg-libinput
%make_build

%install
%make_install



for file in %{buildroot}%{_datadir}/applications/*.desktop ; do
    desktop-file-install \
        --add-category="X-XFCE" \
        --remove-category="XFCE" \
        --delete-original \
        --dir=%{buildroot}%{_datadir}/applications \
        $file
done

mkdir -p %{buildroot}%{_sysconfdir}/skel/.config/autostart
mkdir -p %{buildroot}%{_sysconfdir}/skel/.config/catfish/
cp -a %{_builddir}/%{name}-%{version}/catfish.rc %{buildroot}%{_sysconfdir}/skel/.config/catfish/catfish.rc
cp -a %{_builddir}/%{name}-%{version}/xfce4 %{buildroot}%{_sysconfdir}/skel/.config/
install -m 600 %{_builddir}/%{name}-%{version}/xcape.desktop %{buildroot}%{_sysconfdir}/skel/.config/autostart/xcape.desktop

mkdir -p %{buildroot}%{_sysconfdir}/skel/.local/share
cp -a %{_builddir}/%{name}-%{version}/xfpanel-switch %{buildroot}%{_sysconfdir}/skel/.local/share/

%find_lang xfce4-settings

%files -f xfce4-settings.lang
%license COPYING
%doc AUTHORS ChangeLog NEWS TODO
%config(noreplace) %{_sysconfdir}/xdg/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml
%config(noreplace) %{_sysconfdir}/xdg/autostart/xfsettingsd.desktop
%config(noreplace) %{_sysconfdir}/xdg/menus/xfce-settings-manager.menu
%{_bindir}/xfce4-*-settings
%{_bindir}/xfce4-settings-editor
%{_bindir}/xfce4-settings-manager
%{_bindir}/xfsettingsd
%{_bindir}/xfce4-find-cursor
%{_datadir}/applications/xfce*.desktop
%{_libdir}/xfce4/settings
%{_datadir}/icons/hicolor/128x128/devices/xfce-*.png
%{_sysconfdir}/skel/.config/autostart/xcape.desktop
%{_sysconfdir}/skel/.config/catfish/catfish.rc
%{_sysconfdir}/skel/.config/xfce4
%{_sysconfdir}/skel/.local/share/xfpanel-switch

%changelog
* Fri Jun 28 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.6-2
- Bump to latest version

* Fri Apr 26 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.5-6
- Bump for 30

* Fri Mar 15 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.5-5
- Use Chromium as default browser

* Tue Jan 29 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.5-4
- Add back xfpanel-switch

* Tue Jan 29 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.5-3
- Add patch for xfsettingsd.desktop file to use --replace (fixes theming issues)

* Tue Jan 15 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.5-2
- Sync back with Older pacakge

* Tue Jan 15 2019 JMiahMan <JMiahMan@unity-linux.org> 4.13.5-1
- Resync with Upstream

* Thu Jan 10 2019 JMiahMan <JMiahMan@unity-linux.org> 0.14-4
- Restructure package

* Wed Jan 09 2019 JMiahMan <JMiahMan@unity-linux.org> 0.14-3
- Attempt to simplify 

* Wed Aug 29 2018 JMiahMan <JMiahMan@unity-linux.org> 0.14-2
- Add catfish config

* Mon Jul 30 2018 JMiahMan <JMiahMan@unity-linux.org> 0.14-1
- Update for 28 replace ksuperkey with xcape (in Fedora)
